package Transport;

import java.io.Serializable;

public class Motocycle implements ITransport, Serializable {

	public Motocycle(String brand_name) {
		brand = brand_name;
		head = new Model("", 0);
		head.next = head;
		head.prev = head;

		size = 0;
		lastModified = System.currentTimeMillis();
	}

	private int size = 0;
	private Model head;
	private transient long lastModified;

	private String brand;

	private class Model implements Serializable{
		public Model(String model_name, double price) {
			this.model_name = model_name;
			this.price = price;
		}

		String model_name = null;
		double price = Double.NaN;
		Model prev = null;
		Model next = null;
	}

	private void addNode(Model node) {
		if (node == null)
			throw new IllegalArgumentException();

		head.next.prev = node;
		node.next = head.next;
		head.next = node;
		node.prev = head;

		lastModified = System.currentTimeMillis();
		size++;
	}

	private void removeNode(Model node) {

		node.prev.next = node.next;
		node.next.prev = node.prev;
		lastModified = System.currentTimeMillis();
		size--;
	}

	private Model getNodeByModelName(String name) {
		Model result = head.next;
		while (result != head) {
			if (result.model_name.equals(name))
				break;
			else
				result = result.next;
		}

		if (result == head)
			result = null;
		return result;
	}

	private Model getModelByName(String name) throws NoSuchModelNameException {
		Model node = getNodeByModelName(name);
		if (node != null)
			return node;
		else
			throw new NoSuchModelNameException(name);
	}

	public void renameModel(String old_name, String new_name)
			throws NoSuchModelNameException, DuplicateModelNameException {
		Boolean name_is_unique = getNodeByModelName(new_name) == null;
		if (name_is_unique)
			getModelByName(old_name).model_name = new_name;
		else
			throw new DuplicateModelNameException(new_name);
		lastModified = System.currentTimeMillis();
	}

	public double getModelPrice(String name) throws NoSuchModelNameException {
		return getModelByName(name).price;
	}

	public void setModelPrice(String name, double new_price) throws NoSuchModelNameException {
		checkPrice(new_price);
		getModelByName(name).price = new_price;
		lastModified = System.currentTimeMillis();
	}

	public String[] getModelNames() {
		String[] result = new String[size];
		Model pointer = head.next;
		int i = 0;
		while (pointer != head) {
			result[i] = pointer.model_name;
			pointer = pointer.next;
			i++;
		}
		return result;
	}

	public double[] getModelPrices() {
		double[] result = new double[size];
		Model pointer = head.next;
		int i = 0;
		while (pointer != head) {
			result[i] = pointer.price;
			pointer = pointer.next;
			i++;
		}
		return result;
	}

	public void addNewModel(String name, double price) throws DuplicateModelNameException {
		Boolean name_is_unique = getNodeByModelName(name) == null;
		checkPrice(price);
		if (name_is_unique)
			addNode(new Model(name, price));
		else
			throw new DuplicateModelNameException(name);
	}

	public void deleteModel(String name) throws NoSuchModelNameException {
		removeNode(getModelByName(name));
	}

	public int countModels() {
		return size;
	}

	private void checkPrice(double price) {
		if (price < 0)
			throw new ModelPriceOutOfBoundException();
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String new_brand) {
		if (new_brand.length() > 0) {
			brand = new_brand;
			lastModified = System.currentTimeMillis();
		} else
			throw new IllegalArgumentException("Название марки не может быть пустым");
	}

}
