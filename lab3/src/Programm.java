import Transport.*;
import java.io.*;

public class Programm {

	public static void main(String[] args) throws DuplicateModelNameException {

		// Car car1 = new Car("УАЗ", 3);
		Motocycle car1 = new Motocycle("ALFA");
		ITransport t = car1;

		t.addNewModel("NewModel1", 31);
		t.addNewModel("NewModel2", 31);
		t.addNewModel("NewModel3", 31);

		System.out.println("Original:");
		TransportUtils.printModelsAndPrices(t);
		serializeITransportToFile(t, "object.bin");

		ITransport tn = deserializeITransportFromFile("object.bin");
		System.out.println("Readed:");
		TransportUtils.printModelsAndPrices(tn);
	}

	private static ITransport deserializeITransportFromFile(String path) {

		ITransport result = null;
		File file = null;
		FileInputStream file_stream = null;
		ObjectInputStream obj_stream = null;

		try {
			file = new File(path);
			file_stream = new FileInputStream(file);
			obj_stream = new ObjectInputStream(file_stream);
			result = (ITransport) obj_stream.readObject();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				file_stream.close();
				obj_stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	private static void serializeITransportToFile(ITransport t, String path) {

		File file = null;
		FileOutputStream file_stream = null;
		ObjectOutputStream obj_stream = null;

		try {
			file = new File(path);
			if (file.exists()) {
				file.delete();
			}

			file_stream = new FileOutputStream(file, true);
			obj_stream = new ObjectOutputStream(file_stream);
			obj_stream.writeObject(t);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				file_stream.close();
				obj_stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void testReadAndWriteITransport() {
		Car car1 = new Car("УАЗ", 3);
		// Motocycle car1 = new Motocycle("ALFA");

		ITransport t = car1;

		try {
			t.addNewModel("NewModel", 31);
			t.addNewModel("NewModel2", 31);
			t.addNewModel("NewModel3", 31);
		} catch (DuplicateModelNameException e) {
			e.printStackTrace();
		}

		TransportUtils.printModelsAndPrices(t);
		writeITransportToFile(t, "object.txt");
		ITransport rt = readITransportFromFile("object.txt");
		TransportUtils.printModelsAndPrices(rt);

	}

	private static ITransport readITransportFromFile(String path) {
		ITransport t = null;

		File txt_file = null;
		FileReader file_reader = null;
		try {
			txt_file = new File(path);
			if (!txt_file.exists())
				txt_file.createNewFile();
			file_reader = new FileReader(txt_file);
			t = TransportUtils.readITransport(file_reader);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				file_reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return t;
	}

	private static void writeITransportToFile(ITransport t, String path) {

		File txt_file = null;
		FileWriter file_writer = null;
		try {
			txt_file = new File(path);
			if (!txt_file.exists())
				txt_file.createNewFile();
			file_writer = new FileWriter(txt_file);
			TransportUtils.writeITransport(t, file_writer);

			file_writer.flush();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				file_writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private static void testDescriptions() throws NoSuchModelNameException, DuplicateModelNameException {
		Car car1 = new Car("УАЗ", 3);

		ITransport t = car1;

		t.addNewModel("NewModel", 31);
		t.addNewModel("NewModel2", 31);
		t.addNewModel("NewModel3", 31);

		String serialized = TransportUtils.getITransportDescription(t);
		ITransport nt = TransportUtils.parceDescription(serialized);
		String nserialized = TransportUtils.getITransportDescription(nt);
		System.out.println(serialized);
		System.out.println(nserialized);

	}

}
