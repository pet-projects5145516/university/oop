package Transport;

import java.util.Arrays;
import java.util.StringJoiner;

public class TransportUtils {
	/*
	 * Написать класс со статическими методами таким образом,
	 * чтобы он работал со ссылками типа интерфейса. В классе должен быть метод,
	 * возвращающий среднее арифметическое цен моделей для заданного
	 * Транспортного средства и методы, обеспечивающие вывод на экран всех
	 * моделей и всех цен на модели для заданного Транспортного средства.
	 */

	public static double getAveragePrice(ITransport transport) {
		double result = Double.NaN;
		double[] prices = transport.getModelPrices();

		if (prices.length > 0) {
			double sum = 0;
			for (int i = 0; i < prices.length; i++)
				sum += prices[i];
			result = sum / prices.length;
		}

		return result;
	}

	public static void printPrices(ITransport transport) {
		System.out.println(Arrays.toString(transport.getModelPrices()));
	}

	public static void printModels(ITransport transport) {
		System.out.println(Arrays.toString(transport.getModelNames()));
	}

	public static void printModelsAndPrices(ITransport transport) {
		StringJoiner joiner = new StringJoiner("\n");

		String[] names = transport.getModelNames();
		double[] prices = transport.getModelPrices();

		for (int i = 0; i < names.length; i++)
			joiner.add(names[i] + " " + prices[i]);

		System.out.println(joiner.toString());
	}

}
