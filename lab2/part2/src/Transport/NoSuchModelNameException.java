package Transport;

public class NoSuchModelNameException extends Exception {
	public NoSuchModelNameException(String model_name) {
		super(String.format("Модели с именем <%s> не существует", model_name));
			}
}


