import Transport.*;
import java.util.Arrays;

public class Programm {

	public static void main(String[] args) throws NoSuchModelNameException, DuplicateModelNameException {
		//Car car1 = new Car("УАЗ", 3);
		Motocycle car1 = new Motocycle("ALFA");

		ITransport t = car1;

		TransportUtils.printModelsAndPrices(t);
		System.out.println();

		t.addNewModel("NewModel", 31);
		t.addNewModel("NewModel2", 31);
		t.addNewModel("NewModel3", 31);

		TransportUtils.printModelsAndPrices(t);
		System.out.println();
		String[] models = t.getModelNames();
		System.out.println("Удаление модели: "+ models[0]);
		t.deleteModel(models[0]);

		 models = t.getModelNames();
		TransportUtils.printModelsAndPrices(t);
		System.out.println();


		System.out.println("Цена " + models[1] +"  :  " + t.getModelPrice(models[1]) );
		t.setModelPrice(models[1], 22);
		System.out.println("Цена " + models[1] +"  :  " + t.getModelPrice(models[1]) );

		System.out.println(Arrays.toString(models));
		t.setModelPrice(models[0], -12);

		TransportUtils.printModelsAndPrices(t);
		System.out.println();

		t.renameModel(models[1], "Renamed");

		TransportUtils.printModelsAndPrices(t);
		System.out.println();

		System.out.println(TransportUtils.getAveragePrice(t));
		System.out.println();
	}
}
