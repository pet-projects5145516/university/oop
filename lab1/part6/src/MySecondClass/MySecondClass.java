package MySecondClass;
public class MySecondClass {

	public MySecondClass(int array_size) {
		array = new int[array_size];
		for (int i = 0; i < array_size; i++)
			array[i] = (int) (Math.random() * 100);

	}

	private int[] array;

	public int GetItem(int index) {
		return array[index];
	}

	public double GetAvarage() {
		double sum = 0;
		for (int i = 0; i < array.length; i++)
			sum += array[i];

		return sum / array.length;
	}

	public void SetItem(int index, int value) {
		array[index] = value;
	}

	public void PrintView() {
		String result = "";
		for (int i = 0; i < array.length; i++)
			result += array[i] + ", ";

		System.out.println(result);
	}

}
