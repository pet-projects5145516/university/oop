package Transport;

import java.util.Arrays;
import java.util.StringJoiner;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class TransportUtils {

	public static double getAveragePrice(ITransport transport) {
		double result = Double.NaN;
		double[] prices = transport.getModelPrices();

		if (prices.length > 0) {
			double sum = 0;
			for (int i = 0; i < prices.length; i++)
				sum += prices[i];
			result = sum / prices.length;
		}

		return result;
	}

	public static void printPrices(ITransport transport) {
		System.out.println(Arrays.toString(transport.getModelPrices()));
	}

	public static void printModels(ITransport transport) {
		System.out.println(Arrays.toString(transport.getModelNames()));
	}

	public static void printModelsAndPrices(ITransport transport) {
		StringJoiner joiner = new StringJoiner("\n");

		String[] names = transport.getModelNames();
		double[] prices = transport.getModelPrices();

		for (int i = 0; i < names.length; i++)
			joiner.add(names[i] + " " + prices[i]);

		System.out.println(joiner.toString());
	}

	public static String getITransportDescription(ITransport t) {

		String result = "";
		result += t.getClass().getName() + "\n";
		result += t.getBrand() + "\n";
		result += (Integer.toString(t.countModels())) + "\n";
		result += (Arrays.toString(t.getModelNames())) + "\n";
		result += (Arrays.toString(t.getModelPrices())) + "\n";
		int lenght = result.length() +
				Integer.toString(result.length()).length();
		result = Integer.toString(lenght) + "\n" + result;

		return result;
	}

	public static ITransport parceDescription(String desk) throws DuplicateModelNameException {
		ITransport result = null;
		String[] parts, model_names, model_prices;
		String type, brand_name;

		parts = desk.split("\n");

		type = parts[1];
		brand_name = parts[2];
		final String split_pattern = "\\[|\\]|,|\\s";
		model_names = parts[4].split(split_pattern);
		model_prices = parts[5].split(split_pattern);

		switch (type) {
			case "Transport.Motocycle":
				result = new Motorcycle(brand_name);
				break;
			case "Transport.Car":
				result = new Car(brand_name, 0);
				break;
			default:
				throw new IllegalArgumentException();
		}

		if (model_names.length != model_prices.length)
			throw new IllegalArgumentException("Число название моделей и их цен должно быть одинаковым");

		for (int i = 0; i < model_names.length; i++) {
			if (!model_names[i].isEmpty() && !model_prices[i].isEmpty())
				result.addNewModel(model_names[i], Double.parseDouble(model_prices[i]));
			else if (model_names[i].isEmpty() ^ model_prices[i].isEmpty())
				throw new IllegalArgumentException();
			;
		}

		return result;
	}

	public static void outITransport(ITransport t, OutputStream writer) throws IOException {
		writer.write(getITransportDescription(t).getBytes());
	}

	public static ITransport inITransport(InputStream input) throws IOException {
		ITransport t = null;
		Reader reader;
		reader = new InputStreamReader(input);
		t = readITransport(reader);
		return t;
	}

	public static ITransport readITransport(Reader reader) throws IOException {
		ITransport result = null;

		String lenght_of_line_str = "";
		int digit_code;
		while ((digit_code = reader.read()) < 58 && digit_code > 47)
			lenght_of_line_str += (char) (digit_code);
		int lenght_of_line = Integer.parseInt(lenght_of_line_str);

		char[] chars = new char[lenght_of_line];
		reader.read(chars);
		String description = lenght_of_line_str + "\n" + new String(chars);

		try {
			result = parceDescription(description);
		} catch (DuplicateModelNameException e) {
			throw new IllegalArgumentException("Передаваемые данных не повреждены. Есть дубликаты имен");
		}

		return result;
	}

	public static void writeITransport(ITransport t, Writer writer) throws IOException {
		writer.write(getITransportDescription(t));
	}

}
