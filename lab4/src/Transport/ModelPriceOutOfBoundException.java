
package Transport;

public class ModelPriceOutOfBoundException extends IllegalArgumentException{
	public ModelPriceOutOfBoundException () {
		super("Цена модели должна быть >= 0");
	}
}


