package Transport;

public interface ITransport {

	public void renameModel(String old_name, String new_name) throws NoSuchModelNameException,DuplicateModelNameException;

	public double getModelPrice(String name) throws NoSuchModelNameException;

	public String getTransportType();

	public void setModelPrice(String name, double new_price) throws NoSuchModelNameException;

	public String[] getModelNames();

	public double[] getModelPrices();

	public void addNewModel(String name, double price) throws DuplicateModelNameException ;

	public void deleteModel(String name) throws NoSuchModelNameException;

	public int countModels();

	public String getBrand();

	public void setBrand(String new_brand);
}
