package Transport;

public class DuplicateModelNameException extends Exception {
	public DuplicateModelNameException(String model_name) {
		super(String.format("Имя <%s> не уникально", model_name));
	}
}


