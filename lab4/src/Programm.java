import Transport.*;

public class Programm {

	public static void main(String[] args) throws Exception {
		// testEqualsMethos();
		// testCarClone();
		testMotorcycleClone();
	}

	static void testCarClone() throws Exception {
		Car car1 = new Car("УАЗ", 5);
		Car car2 = (Car) car1.clone();

		System.out.println(car1.equals(car2));

		String[] models = car1.getModelNames();

		car2.addNewModel("ruch", 12);
		System.out.println(!car1.equals(car2));

		car2.deleteModel("ruch");
		System.out.println(car1.equals(car2));

		car2.renameModel(models[1], "renamed");
		System.out.println(!car1.equals(car2));
	}

	static void testMotorcycleClone() throws Exception {
		Motorcycle mot1 = new Motorcycle("УАЗ");
		mot1.addNewModel("first", 12);
		mot1.addNewModel("second", 12);

		Motorcycle mot2 = (Motorcycle) mot1.clone();


		mot1.addNewModel("trird", 10);
		mot2.deleteModel("second");
		mot2.renameModel("first", "renamed");

		System.out.println(mot1);
		System.out.println(mot2);
	}

	static void testEqualsMethos() throws DuplicateModelNameException {

		ITransport t;
		Car car1 = new Car("УАЗ", 0);
		Car car2 = new Car("УАЗ", 0);

		Motorcycle mot1 = new Motorcycle("ALFA");
		Motorcycle mot2 = new Motorcycle("ALFA");

		System.out.println(partTestEqualsMethod(car1, car2));
		System.out.println(partTestEqualsMethod(mot1, mot2));

	}

	static boolean partTestEqualsMethod(ITransport t1, ITransport t2) throws DuplicateModelNameException {
		boolean a, b, c;
		// Пустые объекты
		a = t1.equals(t2);

		// Одинаковые объекты
		t1.addNewModel("NewModel1", 31);
		t1.addNewModel("NewModel2", 31);
		t1.addNewModel("NewModel3", 31);
		t2.addNewModel("NewModel1", 31);
		t2.addNewModel("NewModel2", 31);
		t2.addNewModel("NewModel3", 31);
		b = t1.equals(t2);

		// Разные объекты
		t2.addNewModel("NewModel4", 31);
		c = !t1.equals(t2);

		return a & b & c;

	}

	static void testToStringMethod() throws DuplicateModelNameException {
		Car car = new Car("УАЗ", 3);
		Motorcycle motorcycle = new Motorcycle("ALFA");
		ITransport t;
		t = car;
		t.addNewModel("NewModel1", 31);
		t.addNewModel("NewModel2", 31);
		t.addNewModel("NewModel3", 31);
		System.out.println(t);

		t = motorcycle;
		t.addNewModel("NewModel1", 31);
		t.addNewModel("NewModel2", 31);
		t.addNewModel("NewModel3", 31);
		System.out.println(t);
	}

}
