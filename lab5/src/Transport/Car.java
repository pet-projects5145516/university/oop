package Transport;

import java.util.Arrays;
import java.io.Serializable;

public class Car extends TransportBase implements Serializable, Cloneable {

	public Car(String brand_in, int model_count) {
		brand = brand_in;
		models = new Model[model_count];
		for (int i = 0; i < models.length; i++)
			models[i] = new Model(getRandomModelName(MODEL_NAME_LENGTH), MODEL_DEFAULT_PRICE);
	}

	private String brand;
	private Model[] models;
	private static final int MODEL_NAME_LENGTH = 3;
	private static final int MODEL_DEFAULT_PRICE = 10;

	private class Model implements Serializable {

		public Model(String name, double price) {
			this.name = name;
			this.price = price;
		}

		public String name;
		public double price;

	}

	private String getRandomModelName(int name_lenght) {
		String result = "";
		for (int i = 0; i < name_lenght; i++) {
			char letter = (char) ((int) (Math.random() * 26) + 65);
			result += letter;
		}
		return result;
	}

	public void renameModel(String old_name, String new_name)
			throws NoSuchModelNameException, DuplicateModelNameException {
		checkUniqueOfModelName(new_name);
		int id = checkExistingOfModelName(old_name);
		models[id].name = new_name;
	}

	public double getModelPrice(String name) throws NoSuchModelNameException {
		int id = checkExistingOfModelName(name);
		return models[id].price;
	}

	public void setModelPrice(String name, double new_price) throws NoSuchModelNameException {
		int id = checkExistingOfModelName(name);
		checkPrice(new_price);
		models[id].price = new_price;
	}

	private int getModelID(String name) { // убрать public !!!
		int i = 0;
		while (i < models.length) {
			if (models[i].name == name)
				break;
			else
				i++;
		}
		if (i == models.length)
			i = -1;
		return i;
	}

	public String[] getModelNames() {
		String[] result = new String[models.length];
		for (int i = 0; i < models.length; i++)
			result[i] = models[i].name; // а не ссылка тут передаётся ли?
		return result;
	}

	public double[] getModelPrices() {
		double[] result = new double[models.length];
		for (int i = 0; i < models.length; i++)
			result[i] = models[i].price; // а не ссылка тут передаётся ли?
		return result;
	}

	public void addNewModel(String name, double price) throws DuplicateModelNameException {
		checkUniqueOfModelName(name);
		checkPrice(price);

		Model model = new Model(name, price);
		Model[] new_models = Arrays.copyOf(models, models.length + 1);
		new_models[new_models.length - 1] = model;
		models = new_models;
	}

	public void deleteModel(String name) throws NoSuchModelNameException {
		var id = checkExistingOfModelName(name);
		models[id] = models[models.length - 1];
		models = Arrays.copyOf(models, models.length - 1);
	}

	public int countModels() {
		return models.length;
	}

	private void checkUniqueOfModelName(String name) throws DuplicateModelNameException {
		if (getModelID(name) != -1)
			throw new DuplicateModelNameException(name);
	}

	private int checkExistingOfModelName(String name) throws NoSuchModelNameException {
		int id = getModelID(name);
		if (id == -1)
			throw new NoSuchModelNameException(name);
		return id;
	}

	private void checkPrice(double price) {
		if (price < 0)
			throw new ModelPriceOutOfBoundException();
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String new_brand) {
		if (new_brand.length() > 0)
			brand = new_brand;
		else
			throw new IllegalArgumentException("Название марки не может быть пустым");
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Car newcar = (Car) super.clone();
		// newcar.models = models.clone(); как оказывается без необходимости
		return newcar;
	}


}
