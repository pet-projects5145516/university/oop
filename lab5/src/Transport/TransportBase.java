
package Transport;

import java.util.Arrays;
import java.util.HashMap;
import java.util.StringJoiner;

public abstract class TransportBase implements ITransport {

	@Override
	public Object clone() throws CloneNotSupportedException {

		ITransport copy = null;

		if (this instanceof Motorcycle) {
			copy = new Motorcycle(getBrand());
		} else if (this instanceof Car) {
			copy = new Car(getBrand(), 0);
		} else {
			throw new CloneNotSupportedException();
		}

		String[] names = getModelNames();
		double[] prices = getModelPrices();
		try {
			for (int i = 0; i < names.length; i++) {
				copy.addNewModel(names[i], prices[i]);
			}
		} catch (Exception e) {
			throw new RuntimeException("Ошибка копирование объекта" + e.getMessage());
		}

		return copy;
	}

	@Override
	public String toString() {
		ExtendedStringBuffer b = new ExtendedStringBuffer();

		b.add("Type: %s", getTransportType());
		b.add("Brand: %s", getBrand());
		b.add("Models: %s", Arrays.toString(getModelNames()));
		b.add("Prices: %s", Arrays.toString(getModelPrices()));
		return b.getResult();
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof ITransport) {
			ITransport t = (ITransport) obj;
			if (t.getTransportType().equals(this.getTransportType()) &&
					t.getBrand().equals(this.getBrand()) && (t.countModels() == countModels())) {

				HashMap<String, Double> h1, h2;
				h1 = new HashMap<String, Double>();
				h2 = new HashMap<String, Double>();

				String[] n1, n2;
				double[] p1, p2;
				n1 = getModelNames();
				p1 = getModelPrices();
				n2 = t.getModelNames();
				p2 = t.getModelPrices();

				for (int i = 0; i < countModels(); i++) {
					h1.put(n1[i], p1[i]);
					h2.put(n2[i], p2[i]);
				}
				// Проверить ли поле last_modified у Motorcycle
				result = h1.equals(h2);

			}
		}

		return result;
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	public String getTransportType() {
		return this.getClass().getName();
	}

}

class ExtendedStringBuffer {
	StringJoiner buffer;

	public ExtendedStringBuffer() {
		buffer = new StringJoiner("\n");
	}

	public void add(String line, Object... args) {
		buffer.add(String.format(line, args));
	}

	public String getResult() {
		return buffer.toString();
	}
}
