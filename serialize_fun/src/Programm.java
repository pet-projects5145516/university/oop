import java.io.*;

public class Programm {
	public static void main(String[] args) throws IOException {

		FileOutputStream fos = new FileOutputStream("temp.out");
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		TestSerial ts = new TestSerial();
		oos.writeObject(ts);
		oos.flush();
		oos.close();
	}
}

class TestSerial implements Serializable{
	public byte version = 100;
	public byte count = 0;
}
